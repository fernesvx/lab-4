using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using AvaloniaApplication2.Models;

namespace AvaloniaApplication2.Views;

public partial class MainWindow : Window
{
    private readonly Sound _sound = new("../../../Assets/som1.wav");
    public MainWindow()
    {
        InitializeComponent();
    }

    private void Screen_OnKeyDown(object? sender, KeyEventArgs e)
    {
        if (e.Key == Key.Left)
        {
            Player.Margin = new Thickness(Player.Margin.Left - 25, Player.Margin.Top, 0, 0);
            _sound.PlaySound();
        }
        else if (e.Key == Key.Right)
        {
            Player.Margin = new Thickness(Player.Margin.Left + 25, Player.Margin.Top, 0, 0);
            _sound.PlaySound();
        }
        if(e.Key == Key.Up)
        {
            Player.Margin = new Thickness(Player.Margin.Left, Player.Margin.Top - 25, 0, 0);
            _sound.PlaySound();
        }
        else if (e.Key == Key.Down)
        {
            Player.Margin = new Thickness(Player.Margin.Left, Player.Margin.Top + 25,0,0);
            _sound.PlaySound();
        }
    }
}