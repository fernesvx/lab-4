namespace AvaloniaApplication2.Models;

using System.IO;
using LibVLCSharp.Shared;

public class Sound
{
    private MediaPlayer MediaPlayer { get; }

    public Sound(string file)
    {
        LibVLC libVlc = new LibVLC();
        string filePath = Path.GetFullPath(file);
        
        Media media = new Media(libVlc, filePath);
        MediaPlayer = new MediaPlayer(media);
    }

    public void PlaySound()
    {
        if (MediaPlayer.State == VLCState.Playing)
        {
            return;
        }

        MediaPlayer.Stop();
        MediaPlayer.Play();
    }
}